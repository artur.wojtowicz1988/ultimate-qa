package driver.manager;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;


public class TestBase {


    @BeforeMethod
    public void setUp(){

        DriverManager.getWebDriver();
        DriverUtils.setInitialConfiguration();
        DriverUtils.navigateToPage("https://www.ultimateqa.com/");
    }

    @AfterMethod
    public void tearDown(){
        DriverManager.disposeDriver();

    }
}
