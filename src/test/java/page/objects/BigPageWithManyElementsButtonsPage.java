package page.objects;

import driver.manager.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class BigPageWithManyElementsButtonsPage {

    @FindBy(css="#et-main-area div[class*='et_pb_button_module_wrapper et_pb_button_0_wrapper et_pb_button_alignment_left et_pb_module '] a[class*='et_pb_button et_pb_button_0 et_pb_bg_layout_light']")
    private WebElement button1;
    @FindBy(css="#et-main-area div[class*='et_pb_button_module_wrapper et_pb_button_1_wrapper et_pb_button_alignment_left et_pb_module '] a[class*='et_pb_button et_pb_button_1 et_pb_bg_layout_light']")
    private WebElement button2;
    @FindBy(css="#et-main-area div[class*='et_pb_button_module_wrapper et_pb_button_2_wrapper et_pb_button_alignment_left et_pb_module '] a[class*='et_pb_button et_pb_button_2 et_pb_bg_layout_light']")
    private WebElement button3;
    @FindBy(css="#et-main-area div[class*='et_pb_button_module_wrapper et_pb_button_3_wrapper et_pb_button_alignment_left et_pb_module '] a[class*='et_pb_button et_pb_button_3 et_pb_bg_layout_light']")
    private WebElement button4;
    @FindBy(css="#et-main-area div[class*='et_pb_button_module_wrapper et_pb_button_4_wrapper et_pb_button_alignment_left et_pb_module '] a[class*='et_pb_button et_pb_button_4 et_pb_bg_layout_light']")
    private WebElement button5;
    @FindBy(css="#et-main-area div[class*='et_pb_button_module_wrapper et_pb_button_5_wrapper et_pb_button_alignment_left et_pb_module '] a[class*='et_pb_button et_pb_button_5 et_pb_bg_layout_light']")
    private WebElement button6;
    @FindBy(css="#et-main-area div[class*='et_pb_button_module_wrapper et_pb_button_6_wrapper et_pb_button_alignment_left et_pb_module '] a[class*='et_pb_button et_pb_button_6 et_pb_bg_layout_light']")
    private WebElement button7;
    @FindBy(css="#et-main-area div[class*='et_pb_button_module_wrapper et_pb_button_7_wrapper et_pb_button_alignment_left et_pb_module '] a[class*='et_pb_button et_pb_button_7 et_pb_bg_layout_light']")
    private WebElement button8;
    @FindBy(css="#et-main-area div[class*='et_pb_button_module_wrapper et_pb_button_8_wrapper et_pb_button_alignment_left et_pb_module '] a[class*='et_pb_button et_pb_button_8 et_pb_bg_layout_light']")
    private WebElement button9;


    public BigPageWithManyElementsButtonsPage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }

    public void setButton1() {
        WaitForElement.waitUntilElementIsClickable(button1);
        button1.click();
    }
    public void setButton2() {
        button2.click();
    }
    public void setButton3() {
        button3.click();
    }
    public void setButton4() {
        button4.click();
    }
    public void setButton5() {
        button5.click();
    }
    public void setButton6() {
        button6.click();
    }
    public void setButton7() {
        button7.click();
    }
    public void setButton8() {
        button8.click();
    }
    public void setButton9() {
        button9.click();
    }

}
