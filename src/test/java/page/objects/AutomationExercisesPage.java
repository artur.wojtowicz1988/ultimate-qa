package page.objects;

import driver.manager.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class AutomationExercisesPage {

    @FindBy(css="a[href*='complicated']")
    private WebElement bigPageWithManyElementsLink;

    @FindBy(css="a[href*='sample-application']") // //a[contains(text(), 'Learn')]
    private WebElement learnAutomateAnApplicationThatEvolveOverTime;


    public AutomationExercisesPage(){
        PageFactory.initElements(DriverManager.getWebDriver(),this);
    }

    public void clickBigPageWithManyElements(){
        WaitForElement.waitUntilElementIsClickable(bigPageWithManyElementsLink);
        bigPageWithManyElementsLink.click();
    }

    public void clickLearnHowoAutomateAnApplicationThatEvolvesOverTime(){
        WaitForElement.waitUntilElementIsClickable(learnAutomateAnApplicationThatEvolveOverTime);
        learnAutomateAnApplicationThatEvolveOverTime.click();
    }
}
