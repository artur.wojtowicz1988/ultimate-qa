package page.objects;

import driver.manager.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class BigPageWithManyElementsPage {
    @FindBy(id="et_pb_contact_name_0")
    private WebElement nameField;
    @FindBy(id="et_pb_contact_email_0")
    private WebElement emailField;
    @FindBy(id="et_pb_contact_message_0")
    private WebElement messageField;
    @FindBy(css="#et_pb_contact_form_0 div form div div span")
    private WebElement getCaptcha;
    @FindBy(name="et_pb_contact_captcha_0")
    private WebElement returnCaptcha;
    @FindBy(css="#et_pb_contact_form_0 div form div button")
    private WebElement submitButton;





    public BigPageWithManyElementsPage(){
        PageFactory.initElements(DriverManager.getWebDriver(),this);
    }

    public void typeIntoNameSection(String name) {
        WaitForElement.waitUntilElementIsVisible(nameField);
        nameField.sendKeys(name);
    }

    public void typeIntoEmailSection(String email){
        emailField.sendKeys(email);
    }

    public void typeIntoMessageSection(String message){
        messageField.sendKeys(message);
    }

    public void getCaptchaValueAndReturn(){

        int captchaWynik;
        String dzialanie = getCaptcha.getText();
        System.out.println("działanie w stringu "+dzialanie);
        System.out.println("Length " + dzialanie.length());

        String liczbaa = dzialanie.substring(0,2).replace(" ", "");
        String liczbab = null;// = dzialanie.substring(4,6).replace(" ", "");

        if(dzialanie.length()==5){
            liczbab = dzialanie.substring(4,5).replace(" ","");
        }
        if(dzialanie.length()==6){
            liczbab = dzialanie.substring(4,6).replace(" ","");
        }
        if(dzialanie.length()==7){
            liczbab = dzialanie.substring(4,7).replace(" ","");
        }


        System.out.println(liczbaa + " i " + liczbab);
        int a = Integer.parseInt(liczbaa);
        int b = Integer.parseInt(liczbab);

        captchaWynik = a + b;
        System.out.println(captchaWynik);

        String captchaWynikToString = String.valueOf(captchaWynik);

        returnCaptcha.sendKeys(captchaWynikToString);

    }


    public void clickSubmitButton(){
        submitButton.click();
    }
}
