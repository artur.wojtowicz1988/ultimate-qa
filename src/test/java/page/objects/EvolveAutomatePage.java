package page.objects;


import driver.manager.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import waits.WaitForElement;


public class EvolveAutomatePage {

    @FindBy(name="firstname")
    private WebElement firstNameField;

    @FindBy(id="submitForm")
    private WebElement submitButton;

    @FindBy(css="a[href*='sprint']")
    private WebElement goToNextSprintButton;

    @FindBy(css="div[class='entry-content']>h2")
    private WebElement sprintName;



    public EvolveAutomatePage() {
        PageFactory.initElements(DriverManager.getWebDriver(), this);
    }



    public void typeFirstName(String firstName){
        WaitForElement.waitUntilElementIsVisible(firstNameField);
        firstNameField.sendKeys(firstName);
    }

    public void assertSubmitButtonisDisplayed(){
        WaitForElement.waitUntilElementIsVisible(submitButton);
        Assert.assertTrue(submitButton.isDisplayed());
    }
    public void assertPageUrl(String expectedUrl) {
        String currentUrl = DriverManager.getWebDriver().getCurrentUrl();
        System.out.println("Current URL:" + currentUrl);
        Assert.assertEquals(currentUrl, expectedUrl);
    }
    public void clickSubmitButton(){
        submitButton.click();

    }


    public void clickGoToNextSprintButton(){
        goToNextSprintButton.isDisplayed();
    }

    public void getSprintName(){
        sprintName.getText();
        System.out.println(sprintName);
        //return sprintName.toString();
    }
}
