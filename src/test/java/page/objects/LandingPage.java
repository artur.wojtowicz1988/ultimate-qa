package page.objects;

import driver.manager.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import waits.WaitForElement;

public class LandingPage {
    @FindBy(css="a[href*='automation']")
    private WebElement automationExercisesLink;



    public LandingPage(){
        PageFactory.initElements(DriverManager.getWebDriver(),this);
    }
    public void clickOnAutomationExercises() {
        WaitForElement.waitUntilElementIsClickable(automationExercisesLink);
        automationExercisesLink.click();

    }

}
