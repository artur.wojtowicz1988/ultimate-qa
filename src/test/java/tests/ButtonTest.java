package tests;

import driver.manager.TestBase;
import org.testng.annotations.Test;
import page.objects.AutomationExercisesPage;
import page.objects.BigPageWithManyElementsButtonsPage;
import page.objects.LandingPage;

public class ButtonTest extends TestBase {



    @Test
    public void buttonTest(){

        LandingPage landingPage = new LandingPage();
        landingPage.clickOnAutomationExercises();

        AutomationExercisesPage automationExercisesPage = new AutomationExercisesPage();
        automationExercisesPage.clickBigPageWithManyElements();

        BigPageWithManyElementsButtonsPage bigPageWithManyElementsButtonsPage = new BigPageWithManyElementsButtonsPage();
        bigPageWithManyElementsButtonsPage.setButton1();
        bigPageWithManyElementsButtonsPage.setButton2();
        bigPageWithManyElementsButtonsPage.setButton3();
        bigPageWithManyElementsButtonsPage.setButton4();
        bigPageWithManyElementsButtonsPage.setButton5();
        bigPageWithManyElementsButtonsPage.setButton6();
        bigPageWithManyElementsButtonsPage.setButton7();
        bigPageWithManyElementsButtonsPage.setButton8();
        bigPageWithManyElementsButtonsPage.setButton9();

    }


}
