package tests;


import driver.manager.TestBase;
import org.testng.annotations.Test;
import page.objects.AutomationExercisesPage;
import page.objects.EvolveAutomatePage;
import page.objects.LandingPage;


public class AutomateAnApplicationThatEvolvesOverTimeTest extends TestBase {

    @Test
    public void applicationLifecycleSprint() throws InterruptedException {
        LandingPage landingPage = new LandingPage();
        landingPage.clickOnAutomationExercises();

        AutomationExercisesPage automationExercisesPage  = new AutomationExercisesPage();
        automationExercisesPage.clickLearnHowoAutomateAnApplicationThatEvolvesOverTime();

        EvolveAutomatePage evolveAutomatePage = new EvolveAutomatePage();
        evolveAutomatePage.getSprintName();
        evolveAutomatePage.typeFirstName("franek");
        evolveAutomatePage.assertSubmitButtonisDisplayed();
        evolveAutomatePage.clickSubmitButton();
        evolveAutomatePage.assertPageUrl("https://www.ultimateqa.com/?firstname=franek");


    }

}
