package tests;

import driver.manager.TestBase;

import org.testng.annotations.Test;
import page.objects.AutomationExercisesPage;
import page.objects.BigPageWithManyElementsPage;
import page.objects.LandingPage;

public class RandomStuffTest extends TestBase {


    @Test
    public void randomStuff() {
        LandingPage landingPage = new LandingPage();
        landingPage.clickOnAutomationExercises();

        AutomationExercisesPage automationExercisesPage = new AutomationExercisesPage();
        automationExercisesPage.clickBigPageWithManyElements();

        BigPageWithManyElementsPage bigPageWithManyElementsPage = new BigPageWithManyElementsPage();
        bigPageWithManyElementsPage.typeIntoNameSection("ala");
        bigPageWithManyElementsPage.typeIntoEmailSection("ma@kota.pl");
        bigPageWithManyElementsPage.typeIntoMessageSection("kot ma ale");
        bigPageWithManyElementsPage.getCaptchaValueAndReturn();
        bigPageWithManyElementsPage.clickSubmitButton();


    }

}
